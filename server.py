#!/usr/bin/python
# -*- coding: utf-8 -*-

""" Alex Coninx
    ISIR - Sorbonne Universite / CNRS
    20/03/2020
""" 
class ServerDataError(Exception):
    pass


def get_name(author):
    return (author.nick if author.nick is not None else author.name)


class Faction:
    def __init__(self, nom, section_id, titre_membre="membre", titre_admin="chef"):
        self.nom = nom
        self.section_id = section_id
        self.titre = titre_membre
        self.titre_admin = titre_admin
        self.id_membres = []
        self.id_admins = []

    async def ajoute_membre(self, uid, client):
        if(uid in self.id_membres):
            raise ServerDataError("Le membre %d fait déjà partie de la faction %s" % (uid, self.nom))
        self.id_membres.append(uid)
        await client.add_user_to_section(uid, self.section_id)

    async def retire_membre(self, uid, client, gm=False):
        if(uid not in self.id_membres):
            raise ServerDataError("Le membre %d ne fait pas partie de la faction %s" % (uid, self.nom))
        if(uid in self.id_admins):
            if(not gm):
                raise ServerDataError("Vous ne pouvez pas supprimer de la faction un autre %s" % (self.titre_admin))
            else:
                self.id_admins.remove(uid)
        self.id_membres.remove(uid)
        await client.rm_user_from_section(uid, self.section_id)
    
    async def ajoute_admin(self, uid, client):
        if(uid in self.id_admins):
            raise ServerDataError("Le membre %d est déjà %s de la faction %s" % (uid, self.titre_admin, self.nom))
        if(uid not in self.id_membres):
            await self.ajoute_membre(uid, client)
        self.id_admins.append(uid)

    def retire_admin(self, uid):
        if(uid not in self.id_admins):
            raise ServerDataError("Le membre %d n'est pas %s de la faction %s" % (uid, self.titre_admin, self.nom))
        self.id_admins.remove(uid)

    def est_membre(self, uid):
        return (uid in self.id_membres)

    def est_admin(self, uid):
        return (uid in self.id_admins)

    def display_fancy(self,bot):
        out = "**__Faction : %s__**\n" % self.nom
        out += "__%ss :__\n" % self.titre_admin.capitalize()
        out += "\n".join([("- "+get_name(bot.uid_as_member(uid))) for uid in self.id_admins])
        out += "\n\n"
        out += "__%ss :__\n" % self.titre.capitalize()
        out += "\n".join([("- "+get_name(bot.uid_as_member(uid))) for uid in self.id_membres if uid not in self.id_admins])
        out += "\n-----"
        return out
        


    def todict(self):
        return self.__dict__
    
    def fromdict(self,objdict):
        for k in objdict.keys():
            self.__setattr__(k, objdict[k])


